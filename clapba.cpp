//
// PRL projekt 2: Carry Look Ahead Parallel Binary Adder
// Created by David Spilka (xspilk00) on 2.4.16.
//

#include <mpi.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <math.h>

using namespace std;

#define TAG_D 0
#define TAG_X 1
#define TAG_Y 2

#define D_S 0
#define D_P 1
#define D_G 2

int getResult(int x, int y, int c, bool * overflow)
{
    int result = x + y + c;
    if (result > 1) {
        *overflow = true;
    }

    return result % 2;
}

int getInitD(int x, int y)
{
    if (x == 0 && y == 0) {
        return D_S;
    } else if (x == 1 && y == 1) {
        return D_G;
    } else {
        return D_P;
    }
}

int getUpSweepD(int leftD, int rightD)
{
    if (leftD == D_S || leftD == D_G) {
        return leftD;
    } else {
        return rightD;
    }
}

int getDownSweepD(int rightD, int myD)
{
    if (rightD == D_S || rightD == D_G) {
        return rightD;
    } else {
        return myD;
    }
}

void distributeValues(int leafs, int lastNonLeaf)
{
    char value;
    fstream fin;
    int i = 0;
    bool newLine = false;
    int valueX;
    int valueY;
    string valuesX = "";
    string valuesY = "";

    fin.open("numbers", ios::in);

    while (fin.good()) {
        value = fin.get();
        if(fin.eof()) {
            break;
        }

        if (value == '\n') {
            newLine = true;
            continue;
        }

        if (!newLine) {
            valuesX.append(1, value);
        } else {
            valuesY.append(1, value);
        };
    }

    fin.close();

    for (i = valuesX.length(); i < leafs; i++) {
        valuesX.insert (0, 1, '0');
    }

    for (i = valuesY.length(); i < leafs; i++) {
        valuesY.insert (0, 1, '0');
    }

    for (i = 0; i < leafs; i++) {
        valueX = valuesX.at(i) == '1' ? 1 : 0;
        valueY = valuesY.at(i) == '1' ? 1 : 0;
        MPI_Send(&valueX, 1, MPI_INT, i + lastNonLeaf + 1, TAG_X, MPI_COMM_WORLD);
        MPI_Send(&valueY, 1, MPI_INT, i + lastNonLeaf + 1, TAG_Y, MPI_COMM_WORLD);
    }
}

int main(int argc, char *argv[])
{
    MPI_Status stat;
    int myId;
    int processors;
    int i;

    //MPI INIT
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &processors);
    MPI_Comm_rank(MPI_COMM_WORLD, &myId);

    int leafs = atoi(argv[1]);
    int nonLeafs = processors - leafs;

    int lastLeaf = processors - 1;
    int lastNonLeaf = lastLeaf - leafs;

    int leftChild = myId * 2 + 1;
    int rightChild = myId * 2 + 2;
    int parent = (myId - (myId % 2 == 0 ? 2 : 1)) / 2;

    bool overflow = false;
    int myValueX = 0;
    int myValueY = 0;
    int myValueC = 0;
    int myValueR;
    int myValueD;
    int leftChildValueD;
    int rightChildValueD;

    //K měření času
    /*double start, end;
    MPI_Barrier(MPI_COMM_WORLD);
    start = MPI_Wtime();*/

    if (myId == 0) {
        distributeValues(leafs, lastNonLeaf);
    }

    if (myId > lastNonLeaf) {
        MPI_Recv(&myValueX, 1, MPI_INT, 0, TAG_X, MPI_COMM_WORLD, &stat);
        MPI_Recv(&myValueY, 1, MPI_INT, 0, TAG_Y, MPI_COMM_WORLD, &stat);

        myValueD = getInitD(myValueX, myValueY);
    }

    for (i = 0; i < log2(leafs); i++) {
        if (myId <= lastNonLeaf) {
            MPI_Recv(&leftChildValueD, 1, MPI_INT, leftChild, TAG_D, MPI_COMM_WORLD, &stat);
            MPI_Recv(&rightChildValueD, 1, MPI_INT, rightChild, TAG_D, MPI_COMM_WORLD, &stat);
            myValueD = getUpSweepD(leftChildValueD, rightChildValueD);
        }

        if (myId != 0) {
            MPI_Send(&myValueD, 1, MPI_INT, parent, TAG_D, MPI_COMM_WORLD);
        }
    }

    if (myId == 0) {
        myValueR = myValueD;
        myValueD = D_P;
    }

    for (i = 0; i < log2(leafs); i++) {
        if (myId != 0) {
            MPI_Recv(&myValueD, 1, MPI_INT, parent, TAG_D, MPI_COMM_WORLD, &stat);
        }

        if (myId <= lastNonLeaf) {
            int downSweepD = getDownSweepD(rightChildValueD, myValueD);
            MPI_Send(&downSweepD, 1, MPI_INT, leftChild, TAG_D, MPI_COMM_WORLD);
            MPI_Send(&myValueD, 1, MPI_INT, rightChild, TAG_D, MPI_COMM_WORLD);
        }
    }

    if (myId == 0) {
        MPI_Send(&myValueR, 1, MPI_INT, lastNonLeaf + 1, TAG_D, MPI_COMM_WORLD);
    }

    if (myId > lastNonLeaf && myId < lastLeaf) {
        MPI_Send(&myValueD, 1, MPI_INT, myId + 1, TAG_D, MPI_COMM_WORLD);
    }

    if (myId > lastNonLeaf + 1) {
        MPI_Recv(&myValueD, 1, MPI_INT, myId - 1, TAG_D, MPI_COMM_WORLD, &stat);
    }

    if (myId == lastNonLeaf + 1) {
        MPI_Recv(&myValueD, 1, MPI_INT, 0, TAG_D, MPI_COMM_WORLD, &stat);
    }

    if (myId > lastNonLeaf + 1) {
        MPI_Send(&myValueD, 1, MPI_INT, myId - 1, TAG_D, MPI_COMM_WORLD);
    }

    if (myId > lastNonLeaf && myId < lastLeaf) {
        MPI_Recv(&myValueD, 1, MPI_INT, myId + 1, TAG_D, MPI_COMM_WORLD, &stat);
    }

    if (myId == lastLeaf) {
        myValueD = D_P;
    }

    if (myValueD == D_G) {
        myValueC = 1;
    }

    int result = getResult(myValueX, myValueY, myValueC, &overflow);

    if (myId > lastNonLeaf) {
        cout << myId << ":" << result << endl;
    }

    MPI_Barrier(MPI_COMM_WORLD);

    if (myId == lastNonLeaf + 1)
    {
        if (overflow) {
            cout << "overflow" << endl;
        }
    }

    //K měření času
    /*MPI_Barrier(MPI_COMM_WORLD);
    end = MPI_Wtime();
    if (myId == 0) {
        cerr << end - start << endl;
    }*/

    MPI_Finalize();

    return 0;
}
