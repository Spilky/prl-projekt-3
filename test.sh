#!/bin/bash
#
# PRL projekt 3: Carry Look Ahead Parallel Binary Adder
# Created by David Spilka (xspilk00) on 2.4.16.
#

if [ $# != 1 ]; then
	echo "Invalid parameters!"
	exit 1
fi

processors=$(((2*$1)-1))

mpic++ --prefix /usr/local/share/OpenMPI -o clapba clapba.cpp
mpirun --prefix /usr/local/share/OpenMPI -np $processors clapba $1 $processors
rm clapba