#!/bin/bash
#
# PRL projekt 2: Minimum Extraction sort
# Created by David Spilka (xspilk00) on 10.3.16.
#

#for j in 1 2 4 8 16 32
#do
#    rm "numbers$j"
#    i=1
#    while [ $i -le $j ]
#    do
#        printf $[ RANDOM % 2 ] >> "numbers$j"
#        ((i++))
#    done
#    printf "\n" >> "numbers$j"
#    k=1
#    while [ $k -le $j ]
#    do
#        printf $[ RANDOM % 2 ] >> "numbers$j"
#        ((k++))
#    done
#done

for j in {1..10}
do
    for i in 1 2 4 8 16 32
    do
        echo "------------------- TEST $j - $i ------------------- "
        cp "numbers$i" "numbers"
        processors=$(((2*$i)-1))

        mpic++ --prefix /usr/local/share/OpenMPI -o clapba clapba.cpp
        mpirun --prefix /usr/local/share/OpenMPI -np $processors clapba $i $processors 2>> "stats$i.csv"
        rm clapba
    done
done